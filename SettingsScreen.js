import React from 'react';
import { StyleSheet, Text, View, Button, Switch } from 'react-native';
import { StackNavigator } from 'react-navigation';

const styles = StyleSheet.create({
  settings: {
     margin: 20 
  }
})

const SettingsScreen = () => (
  <View style={{ flex: 1, alignItems: 'center'}}>
    <Text style = {styles.settings}>Profile Setting</Text>
    <Text style = {styles.settings}>Setting Theme</Text>
    <View style= {{ flex: 1, margin: 20}}>
      <Text style = {{flex: 0.8}}> Notification ON/OFF </Text>
      <Switch style = {{flex: 0.2}} />
    </View>
    <Text style = {styles.settings}>Report a Problem </Text>
    <Text style = {styles.settings}>Rate the APP </Text>
    <Text style = {styles.settings}>Term of use </Text>
    <Text style = {styles.settings}>Privacy policy</Text>
    <Text style = {styles.settings}>Log out </Text>
  </View>
);

export default SettingsScreen;