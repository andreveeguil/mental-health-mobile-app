import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import { StackNavigator } from 'react-navigation';

const styles = StyleSheet.create({
  profile: {
     margin: 20 
  }
})

const ProfileScreen = () => (
  <View style={{ flex: 1, alignItems: 'center' }}>
    <Text style = {styles.profile}>Name: Andre</Text>
    <Text style = {styles.profile}>Gender: Male</Text>
    <Text style = {styles.profile}>Profession: Android Developer</Text>
    <Text style = {styles.profile}>Address: Spanish Village</Text>
    <Text style = {styles.profile}>Introduce Yourself: Hi, my name is Andre!</Text>
  </View>
);

export default ProfileScreen;