import React from 'react';
import { View, Text, Button, StyleSheet, ScrollView } from 'react-native';
import { StackNavigator } from 'react-navigation';
import { Card, Header } from 'react-native-elements';


const styles = StyleSheet.create({
    button: {
       margin: 20 
    }
})

class HomeScreen extends React.Component {
    render(){
        const {navigate} = this.props.navigation
        return(
            <ScrollView>
                <Header
                leftComponent = {{icon : 'menu', color: '#fff'}}
                centerComponent = {{text : "Home", style: {color: '#fff'}}}
                />
                    
            <Card title="How is your day today?">
                <Text>How do you feel today?</Text>
                <View style = {styles.button}>
                    <Button color="#F4A460" title="Sad"></Button>
                </View>
                <View style = {styles.button}>
                    <Button color="#ffff00" title="tiring"></Button>
                </View>
                <View style = {styles.button}>
                    <Button color="#FF0000" title="Angry"></Button>
                </View>
                <View style = {styles.button}>
                    <Button color="#FFA500" title="Frustating"></Button>
                </View>
                <View style = {styles.button}>
                    <Button color="#FFC0CB" title="Love"></Button>
                </View>
                <View style = {styles.button}>
                    <Button color="#808080" title="So-So"></Button>
                </View>
                <View style = {styles.button}>
                    <Button color="#0000FF" title="Surprise"></Button>
                </View>
                <View style = {styles.button}>
                    <Button color="#008000" title="Happy"></Button>
                </View>
            </Card>
            </ScrollView>
        )
    }
}

export default HomeScreen;