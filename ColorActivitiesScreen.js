import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import { StackNavigator } from 'react-navigation';
import { ColorPicker } from 'react-native-color-picker'

const ColorActivitiesScreen = () => (
    <View style={{ flex: 1, justifyContent: 'center' }}>
        <ColorPicker
            onColorSelected={color => alert(`Color selected: ${color}`)}
            style={{flex: 1}}
        />
    </View>
);

export default ColorActivitiesScreen;