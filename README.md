# Mental-Health-Mobile-App

## Description
This is the patient side of the mental health platform. A mobile app designed to monitor patients and give feedbacks to the doctor. Supported for both Android and iOS.

## Getting Started
This project use expo for now, future improvements might need specific commands for android and ios.
- Download expo from Play Store or App Store
- `npm install`
- Go to the app folder and `npm start` on terminal
- Open `expo` from mobile device and select 'Scan QR Code'
- Scan the QR Code displayed on terminal
- Your app will be installed on your device

## Testing
Testing is done using `Jest` and `Enzyme`

## TODO
- [ ] Add ESLint and Prettier for better code style
- [ ] Add test code in `Jest` and `Enzyme`
- [ ] Add babel for ES6 support
- [ ] Add webpack if necessary 

## Contact
For questions and inqueries about this repository, please email `andreveeguil@gmail.com`
