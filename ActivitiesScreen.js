import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import { StackNavigator } from 'react-navigation';

const styles = StyleSheet.create({
    button: {
       margin: 20 
    }
})

class ActivitiesScreen extends React.Component{
    render(){
        const {navigate} = this.props.navigation
        return(
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <Text>Activities Screen</Text>
                <View style = {styles.button}>
                    <Button
                    onPress={() => navigate('ShapeActivities')}
                    title="Go to Shape Activities"
                    />
                </View>
                <View style = {styles.button}>
                    <Button
                    onPress={() => navigate('ColorActivities')}
                    title="Go to Color Activities"
                    />
                </View>
                <View style = {styles.button}>
                    <Button
                    onPress={() => navigate('PictureActivities')}
                    title="Go to Picture Activities"
                    />
                </View>
                <View style = {styles.button}>
                    <Button
                    onPress={() => navigate('DrawActivities')}
                    title="Go to Draw Activities"
                    />
                </View>
            </View>
        )
    }
};

export default ActivitiesScreen;