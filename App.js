import React from 'react';
import { StyleSheet, Text, View, Button, Image } from 'react-native';
import { StackNavigator, DrawerNavigator } from 'react-navigation';
import Sidebar from 'react-native-sidebar';
import HomeScreen from './HomeScreen';
import ActivitiesScreen from './ActivitiesScreen';
import CalendarScreen from './CalendarScreen';
import ThoughtCheckerScreen from './ThoughtCheckerScreen';
import ThoughtTrackerScreen from './ThoughtTrackerScreen';
import SettingsScreen from './SettingsScreen';
import ProfileScreen from './ProfileScreen';
import ShapeActivitiesScreen from './ShapeActivitiesScreen';
import ColorActivitiesScreen from './ColorActivitiesScreen';
import PictureActivitiesScreen from './PictureActivitiesScreen';
import DrawActivitiesScreen from './DrawActivitiesScreen';

const App = DrawerNavigator({
    Home: { 
      screen: HomeScreen,
      navigationOptions: {
        drawerLabel : 'Home',
        headerTitle: 'Home',
      },
    },
  Activities: {
    screen: ActivitiesScreen,
    navigationOptions: {
      headerTitle: 'Activities',
    },
  },
  Calendar: {
    screen: CalendarScreen,
    navigationOptions: {
      headerTitle: 'Calendar'
    },
  },
  ThoughtChecker: {
    screen: ThoughtCheckerScreen,
    navigationOptions: {
      headerTitle: 'ThoughtChecker'
    },
  } ,
  ThoughtTracker: {
    screen: ThoughtTrackerScreen,
    navigationOptions: {
      headerTitle: 'ThoughtTracker'
    },
  },
  Settings: {
    screen: SettingsScreen,
    navigationOptions: {
      headerTitle: 'Settings'
    },
  },
  Profile: {
    screen: ProfileScreen,
    navigationOptions: {
      headerTitle: 'Profile'
    }
  },
  ShapeActivities: {
    screen: ShapeActivitiesScreen,
    navigationOptions: {
      headerTitle: 'ShapeActivities'
    }
  },
  ColorActivities: {
    screen: ColorActivitiesScreen,
    navigationOptions: {
      headerTitle: 'ColorActivities'
    }
  },
  PictureActivities: {
    screen: PictureActivitiesScreen,
    navigationOptions: {
      headerTitle: 'PictureActivities'
    }
  },
  DrawActivities: {
    screen: DrawActivitiesScreen,
    navigationOptions: {
      headerTitle: 'DrawActivities'
    }
  }
})



export default App;