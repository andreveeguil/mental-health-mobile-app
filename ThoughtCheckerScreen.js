import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import { StackNavigator } from 'react-navigation';

class ThoughtCheckerScreen extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      first : "I have felt cheerful and in good spirits",
      second: "I have felt calm and relaxed",
      third : "I have felt active and vigorous",
      fourth: "I woke up feeling fresh and rested",
      fifth: "My daily life has been filled with things that interest me",
      now_question : 1,
      now_question_string : "I have felt cheerful and in good spirits"
    }
    this.getNextQuestion = this.getNextQuestion.bind(this)
    this.getPreviousQuestion = this.getPreviousQuestion.bind(this)
  }

  getNextQuestion(){
    if (this.state.now_question == 1){
      this.setState({now_question_string: this.state.second })
      this.setState({now_question: 2 })
    }
    else if (this.state.now_question == 2){
      this.setState({now_question_string: this.state.third })
      this.setState({now_question: 3 })
    }
    else if (this.state.now_question == 3){
      this.setState({now_question_string: this.state.fourth })
      this.setState({now_question: 4 })
    }
    else if (this.state.now_question == 4){
      this.setState({now_question_string: this.state.fifth })
      this.setState({now_question: 5})
    }
    else if (this.state.now_question == 5){
      this.setState({now_question_string: "The Questionnaire has been done for today" })
    }
  }

  getPreviousQuestion(){
    if (this.state.now_question == 2){
      this.setState({now_question_string: this.state.first })
      this.setState({now_question: 1 })
    }
    else if (this.state.now_question == 3){
      this.setState({now_question_string: this.state.second })
      this.setState({now_question: 2 })
    }
    else if (this.state.now_question == 4){
      this.setState({now_question_string: this.state.third })
      this.setState({now_question: 3})
    }
    else if (this.state.now_question == 5){
      this.setState({now_question_string: this.state.fourth })
      this.setState({now_question: 4})
    }
  }

  render(){
    return(
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>{this.state.now_question_string}</Text>
        <Button onPress={this.getNextQuestion} title="Most of time" />
        <Button onPress={this.getNextQuestion} title="All the time" />
        <Button onPress={this.getNextQuestion} title="More than Half time" />
        <Button onPress={this.getNextQuestion} title="Less than Half Time" />
        <Button onPress={this.getNextQuestion} title="No time" />
        <Button onPress={this.getNextQuestion} title="Next" />
        <Button onPress={this.getPreviousQuestion} title="Previous" />
      </View>
    )
  }
}

export default ThoughtCheckerScreen;